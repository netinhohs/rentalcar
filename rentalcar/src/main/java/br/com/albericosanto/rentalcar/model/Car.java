package br.com.albericosanto.rentalcar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "car")
public class Car implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11)
	private Integer id;
	
	@Column(name = "manufacturer", length = 255, nullable = false)
	private String manufacturer;
	
	@Column(name = "model", length = 255, nullable = false)
	private String model;
	
	@Column(name = "model_year", length = 4)
	private int modelYear;
	
	@Column(name = "category")
	private Category category;
	
}
