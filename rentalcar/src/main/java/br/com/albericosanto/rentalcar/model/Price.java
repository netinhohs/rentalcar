package br.com.albericosanto.rentalcar.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "category_price")
public class Price {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11)
	private Integer id;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
	private Category category; 
	
	@Column(name = "week_price")
	private Double weekPrice; 
	
	@Column(name = "weekend_price")
	private Double weekendPrice; 
	
	@Column(name = "loyalty_week_price")
	private Double loyaltyWeekPrice; 
	
	@Column(name = "loyalty_weekend_price")
	private Double loyaltyWeekendPrice;

}
