package br.com.albericosanto.rentalcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.albericosanto.rentalcar.model.Category;


@RepositoryRestResource(path = "category", 
						collectionResourceRel = "category")
public interface ICategoryRepository extends PagingAndSortingRepository<Category, Integer>{

}
