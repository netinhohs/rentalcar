package br.com.albericosanto.rentalcar.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.albericosanto.rentalcar.model.Car;
import br.com.albericosanto.rentalcar.model.Category;

@RepositoryRestResource(path = "car", 
						collectionResourceRel = "car")
public interface ICarRepository extends PagingAndSortingRepository<Car, Integer>{

	Optional<Car> findByCategory(Category cat);
}
