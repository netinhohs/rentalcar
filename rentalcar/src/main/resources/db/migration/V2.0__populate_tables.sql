INSERT INTO `category` (`id`,`name`) 
    VALUES (1,'Compact Hatch'), (2,'Medium Hatch'), (3, 'Sedan'), (4, 'Van'), (5, 'Pickup');
 
INSERT INTO `category_price` (`category_id`, `week_price`, `weekend_price`, `loyalty_week_price`, `loyalty_weekend_price`)
    VALUES (1, '130', '120', '100', '90'), (2, '120', '120', '110', '100'), (3, '140', '110', '120', '80'), (4, '150', '100', '130', '110'), (5, '130', '100', '130', '110');
