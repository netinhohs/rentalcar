CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `model_year` int(4) NOT NULL,
  `category_id` int(11) not null,
  FOREIGN KEY (category_id) REFERENCES category (id),
  PRIMARY KEY (`id`)
);

CREATE TABLE `category_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `week_price` double not null,
  `loyalty_week_price` double not null,
  `weekend_price` double not null,
  `loyalty_weekend_price` double not null,
  FOREIGN KEY (category_id) REFERENCES category (id),
  PRIMARY KEY (`id`) 
);

